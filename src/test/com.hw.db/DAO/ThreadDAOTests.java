package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class threadDAOTests {
    @Test
    @DisplayName("treeSort (since = true desc = true)")
    void treeSortTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
        new PostDAO.PostMapper();
        new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, true);
        
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), any(PostDAO.PostMapper.class), any(), any(), any());
    }

    @Test
    @DisplayName("treeSort (since = true desc = false)")
    void treeSortTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
        new PostDAO.PostMapper();
        new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, false);
        
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"), any(PostDAO.PostMapper.class), any(), any(), any());
    }

}