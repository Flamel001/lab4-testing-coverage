package com.hw.db.DAO;

import com.hw.db.models.Forum;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class postDAOTests {
    private Post postStub;
    private JdbcTemplate mockJdbc;
    @BeforeEach
    @DisplayName("post creation test")
    void createPostTest() {
        postStub = new Post("Flamel", new Timestamp(1337), "forum", "will someone read it?", 1, 1, true);
        mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
        new PostDAO.PostMapper();
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
    }

    @Test
    @DisplayName("SetPost (equal fields)")
    void setPostTest1() {
        Post post = new Post("Flamel", new Timestamp(1337), "forum", "will someone read it?", 1, 1, true);
        PostDAO.setPost(1, post);
        verify(mockJdbc).queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1));
        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    @DisplayName("SetPost (another author)")
    void setPostTest2() {
        Post post = new Post("not Flamel", new Timestamp(1337), "forum", "will someone read it?", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), any(), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost (another message)")
    void setPostTest3() {
        Post post = new Post("Flamel", new Timestamp(1337), "forum", "Wow they read", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), any(), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost (another time)")
    void setPostTest4() {
        Post post = new Post("Flamel", new Timestamp(1488), "forum", "will someone read it?", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), any(), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("SetPost (another author and message)")
    void setPostTest5() {
        Post post = new Post("not Flamel", new Timestamp(1337), "forum", "Wow they read", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), any(), any(), any());
    }

    @Test
    @DisplayName("SetPost (another message and time)")
    void setPostTest6() {
        Post post = new Post("Flamel", new Timestamp(1488), "forum", "Wow they read", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), any(), any(), any());
    }

    @Test
    @DisplayName("SetPost (another author, message and time (everything)")
    void setPostTest7() {
        Post post = new Post("not Flamel", new Timestamp(1488), "forum", "Wow they read", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(postStub);
        PostDAO.setPost(1, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), any(), any(),any(), any());
    }
}