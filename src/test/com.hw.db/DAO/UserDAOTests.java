package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verify;

class userDAOTests {

    @Test
    @DisplayName("Change (empty args)")
    void ChangeTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User(null, null, null, null);
        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }
    @Test
    @DisplayName("Change (with email)")
    void ChangeTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", "email", null, null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    @DisplayName("Change (with fullname)")
    void ChangeTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", null, "fullName", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    @DisplayName("Change (with about)")
    void ChangeTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", null, null, "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    @DisplayName("Change (with email and fullname)")
    void ChangeTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", "email", "fullName", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    @DisplayName("Change (with fullname and about)")
    void ChangeTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", null, "fullName", "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    @DisplayName("Change (with email and about)")
    void ChangeTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", "email", null, "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }

    @Test
    @DisplayName("Change (with email, fullname and about)")
    void ChangeTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("flamel", "email", "fullName", "about");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), (Object) any());
    }
}